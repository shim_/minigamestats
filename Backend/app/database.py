import config

__author__ = 'marvin'

from sqlalchemy import create_engine, MetaData, Table

engine = create_engine(
    'mysql://' + config.username + ':' + config.password + '@' + config.host + ':' + str(
        config.port) + "/" + config.db_name,
    convert_unicode=True, echo=True, pool_size=3, max_overflow=1)

engine.connect()

metadata = MetaData(bind=engine)

table_names = []

for row in engine.execute('SHOW TABLES'):
    table_names.append(row[0])

_tables = {}


def get_game_table(game):
    table_name = to_table_name(game)
    if _tables.has_key(table_name):
        return _tables[table_name]
    else:
        if table_name in table_names:
            table = Table(table_name, metadata, autoload=True)
            _tables[table_name] = table
            return table


def to_table_name(game):
    return config.table_prefix + "_" + game


def to_game_name(table_name):
    if str(table_name).startswith(config.table_prefix + "_"):
        return table_name[len(config.table_prefix) + 1:]
    return table_name

