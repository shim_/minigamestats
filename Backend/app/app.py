__author__ = 'marvin'

import json
from random import randint

from flask import Flask, render_template
from flask.ext.cache import Cache
from sqlalchemy import asc, desc, func, and_

from level_calculator import calculate_level, level_to_points
import database
import config


app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'filesystem', 'CACHE_THRESHOLD': 1000,
                           'CACHE_DIR': '/tmp/minigamestats' + str(randint(10000, 99999))})


@app.route('/')
@cache.cached(timeout=120, key_prefix='games')
def index():
    games = []
    for tbl_name in database.table_names:
        games.append(database.to_game_name(tbl_name))
    return json.dumps(games)


@app.route('/levelparamters')
@app.route('/lvlparms')
def level_parameters():
    return json.dumps({"base": config.base, "threshold": config.threshold})


# @app.route('/ranking/<game>', defaults={'by': 'points', 'limit': 30, 'offset': 0, 'order_by': "DESC"})
# @app.route('/ranking/<game>/<by>', defaults={'limit': 30, 'offset': 0, 'order_by': "DESC"})
# @app.route('/ranking/<game>/<by>/<int:limit>', defaults={'offset': 0, 'order_by': "DESC"})
# @app.route('/ranking/<game>/<by>/<int:limit>/<int:offset>', defaults={'order_by': "DESC"})
@app.route('/ranking/<game>/<by>/<int:limit>/<int:offset>/<order_by>', strict_slashes=False)
# @cache.cached(timeout=30, key_prefix='ranking')
def ranking(game, by, limit, offset, order_by):
    table = database.get_game_table(game)
    if table is None:
        return render_template("error.html", type="Not found", message="Invalid game")
    columns = dict(table.c)
    columns['kd'] = (table.c.kills / table.c.deaths)
    columns['level'] = func.sqrt((table.c.points / ((100 + config.threshold) / 100)) / config.base)
    # return int(level * level * base * (float(100 + threshold) / 100))
    del columns['uuid']
    del columns['iduser']
    del columns['username']
    if table is None or not by in columns:
        return render_template("error.html", type="No such condition", message="Invalid condition")
    rank = offset - 1

    base_query = table.select(and_(table.c.points > 0, table.c.played_games > 4, table.c.points > 0))

    def get_amount():
        return len(base_query.with_only_columns([table.c.iduser]).execute().fetchall())

    amount = get_or_compute('amount_%s' % game, get_amount, [], 600)
    if rank < 0:
        rank = 0

    def fetch_rows(rank):
        rows = []
        if order_by == "DESC":
            result = base_query.order_by(
                desc(columns[by])).limit(limit).offset(offset).execute()
        else:
            rank = amount - offset
            result = base_query.order_by(asc(columns[by])).limit(limit).offset(offset).execute()
        for row in result:
            d_row = dict(row)
            del d_row['iduser']
            if order_by == "DESC":
                rank += 1
            else:
                rank -= 1
            d_row['rank'] = rank
            d_row['level'] = calculate_level(row['points'], config.base, config.threshold)
            rows.append(d_row)
        return {"rows": rows, "rank": rank}

    result = get_or_compute('rows_%s_%s_%s_%d_%d' % (game, by, order_by, limit, offset), fetch_rows, [rank])
    rows = result["rows"]
    rank = result["rank"]

    # return json.dumps(rows, sort_keys=True)
    if config.json_responses:
        return json.dumps({"records": rows, "amount": amount}, sort_keys=True)
    else:
        return render_template("ranking.html", ranking=rows, game=game, request_parameters=locals())


@app.route('/player/search/<string(minlength=3,maxlength=16):username>', defaults={'game': None})
@app.route('/player/search/<game>/<string(minlength=3,maxlength=16):username>')
def search_player(username, game):
    names = cache.get('search_%s_%s' % (game, username))
    if not names:
        names = []
    if len(names) == 0:
        for game_table in database.table_names:
            table = database.get_game_table(database.to_game_name(game_table))
            if table is None or (game is not None and not game == database.to_game_name(game_table)):
                continue
            result = table.select(table.c.username.like("%" + username + "%")).execute()
            for row in result:
                name = row['username']
                if not name in names:
                    names.append(name)
        cache.set('search_%s_%s' % (game, username), names, timeout=300)
    if config.json_responses:
        return json.dumps({"results": names}, sort_keys=True)


@app.route('/player/<username>', defaults={'uuid': None})
@app.route('/player/uuid/<uuid>', defaults={'username': None})
def player(username, uuid):
    profiles = {}
    for game_table in database.table_names:
        table = database.get_game_table(database.to_game_name(game_table))
        if table is None:
            continue
        row = None
        if username is None:
            row = table.select(table.c.uuid == uuid).execute().first()
            username = row['username']
        else:
            row = table.select(table.c.username == username).execute().first()
        if row is None:
            profile = {'played': False}
        else:
            profile = dict(row)
            del profile['iduser']
            if profile['points'] is None:
                profile['points'] = 0
            profile['level'] = calculate_level(profile['points'])
            profile['next_level'] = level_to_points(profile['level'] + 1) - profile['points']
            profile['played'] = True
        profiles[database.to_game_name(game_table)] = profile
    # return json.dumps(profiles, sort_keys=True)
    if config.json_responses:
        return json.dumps(profiles, sort_keys=True)
    else:
        return render_template("profile.html", profiles=profiles, player=username, reqest_paramters=locals())


def get_or_compute(key, func, args=[], timeout=30):
    value = cache.get(key)
    if value:
        return value
    value = func(*args)
    cache.set(key, value, timeout)
    return value

