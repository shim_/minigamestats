__author__ = 'marvin'

import math


def calculate_level(points, base=100, threshold=25):
    return int(math.ceil(math.sqrt((points / (float(100 + threshold) / 100)) / base)))


def points_required(level, base=100, threshold=25):
    return level_to_points(level, base, threshold) - level_to_points(level - 1, base, threshold)


def level_to_points(level, base=100, threshold=25):
    return int(level * level * base * (float(100 + threshold) / 100))


if __name__ == "__main__":
    base = 100
    threshold = 20
    for level in range(1, 20):
        max_points = level_to_points(level, base, threshold)
        print "Level: %d" % (level)
        print "Points to reach the next level: %d" % points_required(level, base, threshold)
        print "Maximum points for this level: %d" % max_points
