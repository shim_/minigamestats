__author__ = 'marvin'

from sqlalchemy import and_, or_

from sqlalchemy import create_engine, MetaData, Table

import config

engine = create_engine(
    'mysql://' + config.permissions_username + ':' + config.permissions_password + '@' + config.permissions_host + ':' + str(
        config.permissions_port) + "/" + config.permissions_db_name,
    convert_unicode=True, echo=True, pool_size=3, max_overflow=1)

engine.connect()

metadata = MetaData(bind=engine)

tbl_memberships = Table("memberships", metadata, autoload=True)

tbl_entities = Table("entities", metadata, autoload=True)


def get_user_groups(name, uuid):
    result = tbl_memberships.select(or_(or_(tbl_memberships.c.member == uuid, tbl_memberships.c.display_name == name),
                                        tbl_memberships.c.member == name)).execute()
    groups = []
    for row in result:
        group = get_group(row.group_id)
        if group is None:
            continue
        else:
            groups.append(group)
    if len(groups) > 0:
        return groups
    return None


def get_group(id):
    result = tbl_entities.select(and_(tbl_entities.c.is_group == 1, tbl_entities.c.id == id())).execute()
    if len(result) > 0:
        return result[0]
    return None