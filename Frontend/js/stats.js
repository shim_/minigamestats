/**
 * Created by marvin on 11.10.14.
 */


(function (window, $) {
    'use strict';
    var apiUrl = "/stats/";
    var entrysPerPage = 30;
    var game = "rush";
    var ranking_table = "#stats table";//"#stats #ranking table";
    var pager_box = "#stats .pagination";//"#stats #ranking .pagination";
    var profile_page = "#stats #profile";
    var images = "/images/stats/";
    var currentPage = 1;
    var class2db = {"playername": "username", "points": "points", "rank": "points", "level": "level", "wins": "wins", "kills": "kills", "kills-deaths": "kd"};
    var levelClasses = {
        "Bauer": {"start": 0, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Rekrut": {"start": 2, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Krieger": {"start": 3, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Scharfschütze": {"start": 4, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Schwertmeister": {"start": 5, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Kriegsherr": {"start": 6, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Reaper": {"start": 7, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Sensei": {"start": 8, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Drachentöter": {"start": 9, "scope": 1, "icon": "/images/stats/classes/$name.png"},
        "Legendär": {"start": 10, "scope": 1, "icon": "/images/stats/classes/$name.png"}
    };

    var urlHash = {"p": currentPage, "d": "r "};
    var profile = $(profile_page);
    var ranking = $(ranking_table);
    var lvlparms = {"base": 800, "threshold": 20}; //TODO: Fetch params

    function listenEvents() {
        $(ranking_table + " th").on("click", function () {
            var arrows = $(this).find(".arrows");
            var col = arrows.parent();
            var arrow = arrows.find("span.active");
            var orderBy = false;
            if (arrow.length === 0) {
                arrow = arrows.find("span.arrow-down");
            }
            arrows.find("span:not(.active)").addClass("active");
            arrow.removeClass("active");
            for (var key in class2db) {
                if (col.hasClass(key)) {
                    orderBy = class2db[key];
                    col.parent().find("th:not(." + key + ") .arrows .active").removeClass("active");
                }
            }
            if (!orderBy) {
                return;
            }
            if (arrow.hasClass("arrow-up")) {
                loadRanking(game, orderBy, "DESC");
            } else {
                loadRanking(game, orderBy, "ASC");
            }
        });
        $(pager_box + " a").on("click", function () {
            var page_btn = $(this);
            currentPage = page_btn.data("page");
            loadRanking(game, getOrder()["sortKey"], getOrder()["sort"]);
        });
    }

    function getOrder() {
        var arrow = $(ranking_table).find("th .arrows span.active");
        var sortKey = "points";
        for (var key in class2db) {
            if (arrow.parent().parent().hasClass(key)) {
                sortKey = class2db[key];
            }
        }
        return{"sortKey": sortKey, "sort": (arrow.hasClass("arrow-up") ? "ASC" : "DESC")};
    }

    function updateUrlHash() {
        var url = "#";
        for (var key in urlHash) {
            url += key + "=" + encodeURIComponent(urlHash[key]) + ",";
        }
        window.location.hash = url.substring(0, url.length - 1);
    }

    function buildUrl(method) {
        var url = (apiUrl + "/" + method).replace(/[\/{2,}]+/g, "/");
        console.log("Build url: " + url);
        return url;
    }

    function updatePager() {
        var pager = $(pager_box);
        var pager_pages = pager.children().length;
        var pages_before = Math.round((pager_pages + 1) / 2);
        var index = 0;
        var offset = currentPage - pages_before;
        urlHash["p"] = currentPage;
        updateUrlHash();
        pager.children().each(function () {
            index = index + 1;
            var btn = $(this);
            var page = offset + index;
            if (offset < 1) {
                page = page + (offset) * -1
            }
            if (page == currentPage) {
                btn.addClass("active");
            } else {
                btn.removeClass("active");
            }
            btn.data("page", page);
            btn.find("span").html(page);
        });
        return;
        for (var i = 0; i < pages_before; i++) {
            var page = copyTemplateJ(pager.find(" .template:not(.active)"));
            page.find("span").html(currentPage - pages_before + i);
            pager.append(page);
            page.data("page", currentPage - pages_before + i);
        }
        var cur_page = copyTemplateJ(pager.find(".template.active"));
        cur_page.find("span").html(currentPage);
        cur_page.data("page", currentPage);
        pager.append(cur_page);
        for (i = 0; i < pages_after; i++) {
            var page = copyTemplateJ(pager.find(" .template:not(.active)"));
            page.find("span").html(currentPage + i + 1);
            page.data("page", currentPage + i + 1);
            pager.append(page);
        }
        pagerListener();
    }

    function copyTemplate(path) {
        return copyTemplateJ($(path));
    }

    function copyTemplateJ(path) {
        var template = $(path);
        var clone = template.clone();
        clone.removeAttr("id");
        clone.removeClass("template");
        return clone;
    }

    function loadRanking(game, orderBy, orderMode) {
        //setTimeout(function () {
        urlHash["d"] = "r";
        urlHash["ob"] = orderBy;
        urlHash["om"] = orderMode;
        updatePager();
        // }, 5);
        ranking.css("display", "block");
        $.getJSON(buildUrl("/ranking/" + game + "/" + orderBy + "/" + entrysPerPage + "/" + (entrysPerPage * (currentPage - 1)) + "/" + orderMode), function (data) {
            ranking = $(ranking_table);
            ranking.find("tr:gt(1)").remove();
            $.each(data, function (key, val) {
                var row = copyTemplate(ranking.find(".template"));
                if (orderBy == "points") {
                    if (val["rank"] < 4) {
                        row.addClass("gold");
                    } else {
                        if (val["rank"] < 11) {
                            row.addClass("silver");
                        }
                    }
                }
                for (var key in val) {
                    if (val.hasOwnProperty(key)) {
                        var clazz = "." + key;
                        row.find(clazz).html(val[key]);
                    }
                }
                var playerName = row.find(".playername span");
                if (devMode()) {
                    var profileLink = document.createElement("a");
                    profileLink.innerHTML = val['username'];
                    profileLink.setAttribute("href", "#");
                    profileLink.setAttribute("data-profile", val['username']);
                    playerName.append(profileLink);
                } else {
                    playerName.html(val['username']);
                }
                playerName.data("uuid", val['uuid']);
                var playerAvatar = row.find(".playeravatar");
                playerAvatar.attr("src", playerAvatar.attr("src").replace("%playername%", val['username']));
                row.find(".kills-deaths").html(val['deaths'] ? (val['kills'] / val['deaths']).toFixed(1) : "-");
                ranking.append(row);
            });
        });
    }

    function loadProfile(player) {
        urlHash["d"] = "p";
        urlHash["p"] = player;
        updateUrlHash();
        $.getJSON(buildUrl("/player/" + player), function (data) {
            var globalPoints = 0;
            var header = profile.find("div.header");
            header.find(".rankoverview .player .playername").html(player);
            var vip = header.find(".rankoverview .player .vip");
            if (isVip(player)) {
                vip.css("display", "block");
            } else {
                vip.css("display", "none");
            }
            $.each(data, function (game, playerProfile) {
                globalPoints += playerProfile["points"];
                var gameProfile = copyTemplate(profile.find("div.games .game.template"));
                gameProfile.find(".gamename").html(game);
                gameProfile.find(".gameicon").attr("src", images + "games/" + game + ".png");
                var currentClass;
                for (var name in levelClasses) {
                    var lClass = levelClasses[name];
                    if (playerProfile["level"] <= lClass["start"] + lClass["scope"]) {
                        currentClass = lClass;
                        currentClass["name"] = name;
                    }
                }
                gameProfile.find(".levelicon").attr("src", currentClass["icon"]);
                gameProfile.find(".levelicon").attr("alt", currentClass["name"]);
                gameProfile.find(".levelname").html(currentClass["name"]);
                gameProfile.find(".level").html(playerProfile["level"]);
                displayProgress(gameProfile.find(".progress"), playerProfile["points"], playerProfile["next_level"] + playerProfile["points"]);

            });
            var levelIcons = header.find(".rankoverview .levelicons");
            levelIcons.find("div img:not(.template)").remove();
            for (var name in levelClasses) {
                var lClass = levelClasses[name];
                var icon = copyTemplateJ(levelIcons.find(".template.levelicon"));
                icon.attr("src", lClass["icon"].replace("$name", name));
                icon.attr("alt", name);
                if (calculateLevel(globalPoints) <= lClass["start"] + lClass["scope"]) {
                    icon.addClass("reached");
                }
                levelIcons.append(icon);
            }
        });
    }

    function displayProgress(progressBar, progress, total) {
        var percent = (progress / total) * 100;
        progressBar.find(".bar .completed").css("width", percent + "%");
        var textual = progressBar.find(".textual");
        textual.find(".completed").html(progress);
        textual.find(".total").html(total);
    }

    function calculateLevel(points) {
        return Math.floor(Math.sqrt((points / ((100 + lvlparms["threshold"]) / 100)) / lvlparms["base"]));
    }

    function isVip(player) {
        return true;
    }

    function devMode() {
        return document.cookie.indexOf("statsDev") > -1;
    }

    $(document).ready(function () {
        loadRanking(game, "points", "DESC");
        listenEvents();
    });
})(window, jQuery);